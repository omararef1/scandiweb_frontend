import React from 'react';

const Product = (props) => {
    const {attributes} = props;
    return ( 
        <div className='col my-2'>
            <div className='card myCard'>
                <div className='card-header'>
                    <input className='form-check-input delete-checkbox' type='checkbox' value='' id='flexCheckDefault' onChange={()=> props.add(attributes.SKU)}></input>

                </div>
                <div className='px-1 py-4'>
                    <h1 className='display-5 text-center'>{attributes.SKU}</h1>
                    <h1 className='display-5 text-center'>{attributes.name}</h1>
                    <h1 className='display-5 text-center'>{attributes.price}$ </h1>
                    {attributes.weight && <h1 className='display-5 text-center'>weight: {attributes.weight} kg</h1>}
                    {attributes.size && <h1 className='display-5 text-center'>size: {attributes.size} MB</h1>}
                    {attributes.length && <h1 className='display-5 text-center'>Dimension: {attributes.length}x{attributes.width}x{attributes.height}</h1>}

                </div>
            </div>
        </div>)
};

export default Product;

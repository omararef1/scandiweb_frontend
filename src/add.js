import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";


import axios from 'axios';

const Add = () => {

    let navigate = useNavigate();

    const originalForm = [
        "SKU",
        "name",
        "price",
    ]

    const [form, setForm] = useState({});
    const [type,setType] = useState("")
   
    const onChangeHandler =  (e) => {

        let id = e.target.id;
        if (id === "sku") {
            id = "SKU"
        }
        console.log(id, e.target.value)
       
        setForm(prevState => ({
            ...prevState,
            [id]: e.target.value
        }));
       

    }
    const [error, setError] = useState();

    const [typeForm, settypeForm] = useState();
    const typeDict = {
        "dvd": <>
            <div className="row align-items-center my-3">
                <div className="col-1">
                    <label htmlFor='sku'>size  </label>
                </div>
                <div className="col-4">
                    <input type="number" className="form-control" id="size" placeholder="please specify size" onChange={(e) => onChangeHandler(e)} required></input>
                </div>
            </div>
            <div className="row align-items-center my-2">
                <div className="col-5 offset-1">
                    <p >please specify the size of the Whole DVD in MBs</p>
                </div>
            </div>
        </>,
        "book": <>
            <div className="row align-items-center my-3">
                <div className="col-1">
                    <label htmlFor='sku'>weight (KG)  </label>
                </div>
                <div className="col-4">
                    <input type="number" className="form-control" id="weight" placeholder="please specify weight" onChange={(e) => onChangeHandler(e)} required></input>
                </div>
            </div>
            <div className="row align-items-center my-2">
                <div className="col-5 offset-1">
                    <p >please specify the weight of the book you want to submit in KG </p>
                </div>
            </div>
        </>
        ,
        "furniture": <>
            <div className="row align-items-center my-2">
                <div className="col-1">
                    <label htmlFor='sku'>height (CM) </label>
                </div>
                <div className="col-4">
                    <input type="number" className="form-control" id="height" placeholder="please specify height" onChange={onChangeHandler} required></input>
                </div>
            </div>
            <div className="row align-items-center my-2">
                <div className="col-1">
                    <label htmlFor='sku'>width (CM) </label>
                </div>

                <div className="col-4">
                    <input type="number" className="form-control" id="width" placeholder="please specify width" onChange={onChangeHandler} required></input>
                </div>
            </div>
            <div className="row align-items-center my-2">
                <div className="col-1">
                    <label htmlFor='sku'>Length (CM) </label>
                </div>
                <div className="col-4">
                    <input type="number" className="form-control" id="length" placeholder="please specify length" onChange={onChangeHandler} required></input>
                </div>

            </div>
            <div className="row align-items-center my-2">
                <div className="col-5 offset-1">
                    <p >please specify the dimensions of the furniture length, width and Height in CM  </p>
                </div>
            </div>

        </>
    }
    const changeForm = (e) => {
        settypeForm(typeDict[e.target.value])
        const value = e.target.value
        setType(value);
        let changingForm = {}
        
        originalForm.forEach((key, value) => {
            
            changingForm[key] = form[key];
            console.log(key, changingForm[key])

        })

        setForm({...changingForm});
        console.log(form)

      

    }

    const onSubmitHandler = () => {
        if (type==="") {
            setError("type is not chosen please choose the type of the product");
            return 

        }
        let myForm = {...form , "type":type}; 
        
        console.log(myForm);
        axios.post(
            "https://gargantuan-bushes.000webhostapp.com/index.php/products",
            JSON.stringify(myForm)
        ).then((res) => {
            
            console.log(res.data)
            navigate("/")
        }).catch((e) => {
            console.log(e);
        })


    }
    return (
        <div className="container mt-3 px-1">
            <div className="row align-items-center">
                <div className="col-4 ">
                    <h2 className="display-3">Product Add</h2>
                </div>
                <div className="col-1 offset-5 ">
                    <button className="btn  btn-primary" type="submit" form="product_form" >Save</button>
                </div>
                <div className="col-2">
                    <a id="delete-product-btn" className="btn btn-outline-danger" href='/'>Cancel</a>
                </div>
                <hr />
            </div>
            <div className="container" >
                <p className='display-5 text-danger'>{error}</p>
                <form id='product_form' onSubmit={(e) => {  e.preventDefault(); onSubmitHandler();  }}>
                    <div className="row align-items-center my-3">
                        <div className="col-1">
                            <label htmlFor='sku'>SKU  </label>
                        </div>
                        <div className="col-4">
                            <input type="text" className="form-control" id="sku" placeholder="please enter SKU" onChange={onChangeHandler} required></input>
                        </div>
                    </div>
                    <div className="row align-items-center my-3">
                        <div className="col-1">
                            <label htmlFor='sku'>name  </label>
                        </div>
                        <div className="col-4">
                            <input type="text" className="form-control" id="name" placeholder="please enter name" onChange={onChangeHandler} required></input>
                        </div>
                    </div>
                    <div className="row align-items-center my-3">
                        <div className="col-1">
                            <label htmlFor='sku'>price($)  </label>
                        </div>
                        <div className="col-4">
                            <input type="number" className="form-control" id="price" placeholder="please enter price" onChange={onChangeHandler} required></input>
                        </div>
                    </div>
                    <div className="row align-items-center my-3">
                        <div className="col-1">
                            <label >Type </label>
                        </div>
                        <div className="col-4">
                            <select type="text" className="form-control" id="productType" onChange={(e) => {changeForm(e)}} required>
                                <option selected={true} disabled >select the type</option>
                                <option value="dvd" id='DVD'>DVD</option>
                                <option value="furniture" id='Furniture'>Furniture</option>
                                <option value="book" id='Book'>Book</option>
                            </select>
                        </div>
                    </div>
                    {typeForm}
                </form>
            </div>
        </div>
    );
};

export default Add;

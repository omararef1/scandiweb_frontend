import './App.css';
import { useEffect, useState } from 'react'
import axios from 'axios';
import Product from './Product';

function App() {

  let productsToDelete = [];
  const add = (id) => {
    if (productsToDelete.includes(id)) {
      productsToDelete = productsToDelete.filter((elems) => {
        return (id !== elems)
      })
    } else {
      productsToDelete.push(id);
    }
    console.log(productsToDelete);
  }


  let [products, setProducts] = useState();
  useEffect(() => {
    axios.get("https://gargantuan-bushes.000webhostapp.com/index.php/products").then(
      (res) => {
        console.log(res.data);
        setProducts(res.data);
      }
    ).catch(() => {
      console.log("ERROR");
    })
  }, [])

  const massDelete = () => {
    axios.post("https://gargantuan-bushes.000webhostapp.com/index.php/deleteProduct",
      JSON.stringify(productsToDelete)).then((res) => {
        setProducts(res.data);
      }).catch(() => {
        console.log("ERROR");
      })
  }

  return (
    <div className="container mt-3 px-1">
      <div className="row align-items-center">
        <div className="col-4 ">
          <h2 className="display-3"> Product List</h2>
        </div>
        <div className="col-1 offset-5 ">
          <a className="btn  btn-primary" href="/add-product">ADD</a>
        </div>
        <div className="col-2">
          <button id="delete-product-btn" className="btn btn-outline-danger" onClick={() => { massDelete() }} value="MASS DELETE">MASS DELETE</button>
        </div>
        <hr />

        <div className='row row-cols-4'>
          {products && products.map((product) => {
            return <Product
              key={product.SKU}
              attributes={product}
              add={add} />
          })}
        </div>
      </div>
    </div>
  );
}

export default App;
